@extends('frontend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">           
            <div class="table-responsive">	
                <div class="col-sm-12"> 
		        	<table class="table table-striped table-bordered table-condensed table-hover">
		        		<tr>
		        			<th>#</th>
		        			<th>Result</th>
		        			<th>Doctor</th>
		        			<th><center>Date</center></th>
		        			<th colspan="3"><center><b>Prescription</b></center></th>
		        		</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($results as $key => $result)
								<tr> 
									<td> {{ $key + 1 }}</td>
									<td> {{ $result->type->name }} </td>
									<td> {{ $result->doctor->name }} </td>
									<td> <center>{{ $result->created_at }}</center></td>
									<td>
										<center>
											<a href="{{ route('frontend.user.results.show',$result->id)}}">
												<i class="fas fa-eye"></i>
											</a>
										<center>
									</td>	
								</tr>
							@endforeach
		        		</tbody>
		        		<tfooter>
		        			<tr>
		        				<td colspan="7"> {{ $results->links()}} </td>
		        			</tr>
		        		</tfooter>
		        	</table>
				</div>
			</div>
	</div>
</div>
@endsection
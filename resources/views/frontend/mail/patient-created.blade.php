@component('mail::message')
# {{$user->first_name }} {{ $user->last_name}}

This patient has been created successfully.

@component('mail::button', ['url' => ''])
View patient
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

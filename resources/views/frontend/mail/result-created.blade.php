@component('mail::message')
# Sickness : {{ $result->type->name }}

<p> Message : {{ $result->message}}</p>

@component('mail::button', ['url' => ''])
View
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# {{ $drug->name }}

{{ $drug->name }} has been created successfully.

@component('mail::button', ['url' => ''])
View drug
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

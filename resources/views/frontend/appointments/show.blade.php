@extends('frontend.layouts.app')

@section('content')
<div class="card">
	<div class="card-body">
    		<div class="row col-md-12">
		        <table class="table table-striped table-bordered">
		           	<tr>
		        		<td align="right"><b>Message</b>  </td>
		        		<td>{{ $appointment->message }}</td>
		        	</tr>
		        	
		        	<tr>
		        		<td align="right"><b>Appointment Date</b></td>
		        		<td>{{ $appointment->appointment_date }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Medication</b></td>
		        		<td>{{ $appointment->medication }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Symptom</b></td>
		        		<td>{{ $appointment->symptom->name }}</td>
		        	</tr>

		        </table>

	    	</div>

	    </div>

	</div>
@endsection
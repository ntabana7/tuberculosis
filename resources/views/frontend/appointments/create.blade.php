@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Create Appointment') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('frontend.user.appointments.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="appointment_date" class="col-md-2 col-form-label text-md-right">{{ __('Appointment Date') }}</label>

                            <div class="col-md-6">
                                <input id="appointment_date" type="date" class="form-control{{ $errors->has('appointment_date') ? ' is-invalid' : '' }}" name="appointment_date" value="{{ old('appointment_date') }}" required>

                                @if ($errors->has('appointment_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('appointment_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-2 col-form-label text-md-right">{{ __('message') }}</label>

                            <div class="col-md-6">
                                <input id="message" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{ old('message') }}" required autofocus>

                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="medication" class="col-md-2 col-form-label text-md-right">{{ __('Medication') }}</label>

                            <div class="col-md-6">
                                <input id="medication" type="medication" class="form-control{{ $errors->has('medication') ? ' is-invalid' : '' }}" name="medication" value="{{ old('medication') }}" required>

                                @if ($errors->has('medication'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medication') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="symptom" class="col-md-2 col-form-label text-md-right">{{ __('Symptom') }}</label>

                            <div class="col-md-6">
                                <input id="symptom" type="text" class="form-control{{ $errors->has('symptom') ? ' is-invalid' : '' }}" name="symptom_id" required>

                                @if ($errors->has('symptom'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('symptom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

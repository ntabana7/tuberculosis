@extends('frontend.layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">           
                <div class="table-responsive">  
                    <div class="col-sm-12"> 
                <table class="table table-striped table-bordered table-condensed table-hover">
               
                    <tr>
                        <th>#</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Message</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($appointments as $key => $appointment)
                            <tr> 
                                <td> {{ $key + 1 }}</td>
                                <td> {{ $appointment->appointment_startdate }} </td>
                                <td> {{ $appointment->appointment_enddate }} </td>
                                <td> {{ $appointment->message }} </td>  
                            </tr>
                        @endforeach
                    </tbody>
                    <tfooter>
                    <tr>
                         <td colspan="7"> {{ $appointments->links()}} </td>
                    </tr>
                    </tfooter>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection
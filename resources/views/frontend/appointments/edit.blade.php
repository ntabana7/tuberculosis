@extends('frontend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Update Appointment') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('frontend.user.appointments.update', $appointment->id) }}">
                        @csrf
                        @method('PATCH')

                        <div class="form-group row">
                            <label for="message" class="col-md-2 col-form-label text-md-right">{{ __('Firstname') }}</label>

                            <div class="col-md-6">
                                <input id="message" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{ $appointment->message }}" required autofocus>

                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="medication" class="col-md-2 col-form-label text-md-right">{{ __('medication') }}</label>

                            <div class="col-md-6">
                                <input id="medication" type="text" class="form-control{{ $errors->has('medication') ? ' is-invalid' : '' }}" name="medication" value="{{ $appointment->medication }}" required autofocus>

                                @if ($errors->has('medication'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medication') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

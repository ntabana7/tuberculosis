@extends('frontend.layouts.app')

@section('content')
<div class="card">
	<div class="card-body">

	        <div class="col-md-12">

				<div class="responsive">

					<table class="table table-striped table-bordered">
						<tr>
							<td align="right">Name</td>
							<td>{{ $prescription->user->first_name .' '. $prescription->user->last_name  }}</td>
						</tr>

						<tr>
							<td align="right">Prescription</td>
							<td>{{ $prescription->drug->name }}</td>
						</tr>
						<tr>
							<td align="right">Start At</td>
							<td> {{ $prescription->start_at }} </td>
						</tr>
						<tr>
							<td align="right">Reorder At</td>
							<td> {{ $prescription->reorder_at }} </td>
						</tr>
						<tr>
							<td align="right">Instruction</td>
							<td> {{ $prescription->instruction }} </td>
						</tr>

						<tr>
							<td align="right">Created At</td>
							<td>{{ $prescription->created_at }}</td>
						</tr>
					</table>
					
				</div>

			</div>
	</div>
</div>
@endsection
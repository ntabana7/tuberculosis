@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
    	<h1><center><h1>List of Appointments</h1></center></h1>
        <div class="row"> 
	        <div class="table-responsive"> 
	            <div class="col-sm-6"> 		    		
			        
			        	<p><b>Username</b>:{{ $patient->username }}</p>
			        	
			        	<p><b>Firstname</b>: {{ $patient->firstname }} </p>

			        	<p><b>Surname</b>:	<td>{{ $patient->lastname }}</td></p>	

		    	</div>

		    	<div class="col-md-12" id="appointment">
	        	
		        	<table class="table table-striped table-bordered">
		        		<thead>
		        			<tr>
			        			<th>#</th>
			        			<th>Message</th>
			        			<th>Medication</th>
			        			<th>Symptom</th>
			        			<th>Appointment Date</th>
			        			<td colspan="3"><center><b>Action</b></center></td>
			        		</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($patientAppointments as $key => $appointment)
								<tr> 
									<td> {{ $key + 1 }}</td>
									<td> {{ $appointment->message }} </td>
									<td> {{ $appointment->medication }} </td>
									<td> {{ $appointment->symptom->name }} </td>
									<td> {{ $appointment->appointment_date }} </td>			
									<td><center>{!! $appointment->action_buttons !!}</center></td>
								</tr>
							@endforeach
		        		</tbody>
		        		<tfoot>
		        			<tr>
		        				<td colspan="8">
		        					{{ $patientAppointments->links() }}
		        				</td>
		        			</tr>
		        		</tfoot>
		        	</table>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection
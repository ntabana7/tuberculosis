@extends('backend.layouts.app')

@section('content')
<div class="card">
	<div class="card-body">

	        <div class="col-md-12">
				<div class="row">

					<table class="table table-striped table-bordered">
						<tr>
							<td align="right">Name</td>
							<td>{{ $type->name }}</td>
						</tr>
					</table>
					
				</div>

			</div>
	</div>
</div>
@endsection
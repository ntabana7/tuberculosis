@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
        	<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        			<td colspan="5">
        				<a href="{{ route('admin.types.create') }}" class="btn btn-success">Create</a>
        			</td>
        		</tr>
        		<tr>
        			<th>#</th>
        			<th>Names</th>
        			<th colspan="3"><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($types as $key => $type)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $type->name }} </td>
							<td><center>{!!  $type->action_buttons !!}</center></td>
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="5"> {{ $types->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
			
		</div>
	</div>
</div>
@endsection
<div class="col">          
    <div class="table-responsive">  
        <div class="col-sm-12"> 
            <table class="table table-striped table-bordered table-condensed table-hover">
                <tr>
                    <th colspan="7">
                        <a href="{{route('admin.results.create',['userId' => $user->id])}}">
                            <span class="btn btn-success" style="color:white">Add Result</span>
                        </a>
                    </th>
                </tr>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Message</th>
                    <th>Doctor</th>
                    <th>Date</th>
                    <th colspan="3"><center><b>Action</b></center></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user->results as $key => $result)
                        <tr> 
                            <td> {{ $key + 1 }}</td>
                            <td> {{ $result->type->name }} </td>
                            <td> {{ substr($result->message, 0,70) }} ... </td>
                            <td> {{ $result->doctor->name }} </td>
                            <td> {{ $result->created_at }} </td>
                            <td><center>{!! $result->action_buttons !!}</center></td>
                            
                        </tr>
                    @endforeach
                </tbody>
                <tfooter>
                <tr>
                    {{-- <td colspan="7"> {{ $user->results->links()}} </td>--}}                  
                </tr>
                </tfooter>
            </table>
        </div>
    </div>
</div>
<div class="col">          
    <div class="table-responsive">  
        <div class="col-sm-12"> 
            <table class="table table-striped table-bordered table-condensed table-hover">
                <tr>
                    <th colspan="7">
                        <a href="{{route('admin.appointments.create',['userId' => $user->id])}}">
                            <span class="btn btn-success" style="color:white">Add Appointment</span>
                        </a>
                    </th>
                </tr>
                <tr>
                    <th>#</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Message</th>
                    <th colspan="3"><center><b>Action</b></center></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user->appointmentsByPatient as $key => $appointment)
                        <tr> 
                            <td> {{ $key + 1 }}</td>
                            <td> {{ $appointment->appointment_startdate }} </td>
                            <td> {{ $appointment->appointment_enddate }} </td>
                            <td> {{ $appointment->message }} </td>
                            <td><center>{!! $appointment->action_buttons !!}</center></td>     
                        </tr>
                    @endforeach
                </tbody>
                <tfooter>
                <tr>
                    {{-- <td colspan="7"> {{ $user->results->links()}} </td>--}}                  
                </tr>
                </tfooter>
            </table>
        </div>
    </div>
</div>
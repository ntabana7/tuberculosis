@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.view'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.users.management')
                    <small class="text-muted">@lang('labels.backend.access.users.view')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true">
                            <i class="fas fa-user"></i> @lang('labels.backend.access.users.tabs.titles.overview')
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#result" role="tab" aria-controls="result" aria-expanded="true">
                            <i class="fas fa-list"></i> @lang('labels.backend.access.users.tabs.titles.result')
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#prescription" role="tab" aria-controls="prescription" aria-expanded="true">
                            <i class="fas fa-list"></i> @lang('labels.backend.access.users.tabs.titles.prescription')
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#appointment" role="tab" aria-controls="appointment" aria-expanded="true">
                            <i class="fas fa-calendar-alt"></i> @lang('labels.backend.access.users.tabs.titles.appointment')
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                        @include('backend.auth.user.show.tabs.overview')
                    </div><!--tab-->

                    <div class="tab-pane" id="result" role="tabpanel" aria-expanded="true">
                        @include('backend.auth.user.show.tabs.result')
                    </div><!--tab-->

                    <div class="tab-pane" id="prescription" role="tabpanel" aria-expanded="true">
                        @include('backend.auth.user.show.tabs.prescriptions')
                    </div><!--tab-->

                    <div class="tab-pane" id="appointment" role="tabpanel" aria-expanded="true">
                        @include('backend.auth.user.show.tabs.appointments')
                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>@lang('labels.backend.access.users.tabs.content.overview.created_at'):</strong> {{ timezone()->convertToLocal($user->created_at) }} ({{ $user->created_at->diffForHumans() }}),
                    <strong>@lang('labels.backend.access.users.tabs.content.overview.last_updated'):</strong> {{ timezone()->convertToLocal($user->updated_at) }} ({{ $user->updated_at->diffForHumans() }})
                    @if($user->trashed())
                        <strong>@lang('labels.backend.access.users.tabs.content.overview.deleted_at'):</strong> {{ timezone()->convertToLocal($user->deleted_at) }} ({{ $user->deleted_at->diffForHumans() }})
                    @endif
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
    	<h1><center><h1>List of All Prescriptions</h1></center></h1>
        <div class="row"> 
	        <div class="table-responsive"> 
	            <div class="col-sm-6"> 		    		
			        			        	
			        	<p><b>Firstname</b>: {{ $user->first_name }} </p>

			        	<p><b>Surname</b>:	<td>{{ $user->last_name }}</td></p>	

		    	</div>

           
		    	<div class="col-md-12" id="prescrition">		
					<table class="table table-striped table-bordered table-condensed table-hover">
		        		<thead>
		        		<tr>
		        			<th>#</th>
		        			<th>Patient</th>
		        			<th>Start At</th>
		        			<th>Reorder At</th>
		        			<th colspan="3"><center><b>Action</b></center></th>
		        		</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($patientPrescriptions as $key => $prescription)
								<tr> 
									<td> {{ $key + 1 }}</td>
									<td> {{ $prescription->user->first_name .' '. $prescription->user->last_name }} </td>
									<td> {{ $prescription->start_at }} </td>
									<td> {{ $prescription->reorder_at }} </td>
									<td><center>{!! $prescription->action_buttons !!}</center></td>
								</tr>
							@endforeach
		        		</tbody>
		        		<tfooter>
		        			<tr>
		        				<td colspan="7"> {{ $patientPrescriptions->links()}} </td>
		        			</tr>
		        		</tfooter>
		        	</table>
		    	</div>

			</div>
		</div>
	</div>
</div>


@endsection
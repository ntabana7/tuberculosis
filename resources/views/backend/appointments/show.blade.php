@extends('backend.layouts.app')

@section('content')
<div class="card">
	<div class="card-body">
    		<div class="row col-md-12">
		        <table class="table table-striped table-bordered">
		        	<tr>
		        		<td align="right"><b>Patient</b></td>
		        		<td>{{ $appointment->patient->full_name }}</td>
		        	</tr>
		        	
		        	<tr>
		        		<td align="right"><b>Start Date</b></td>
		        		<td>{{ $appointment->appointment_startdate }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>End Date</b></td>
		        		<td>{{ $appointment->appointment_enddate }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Message</b>  </td>
		        		<td>{{ $appointment->message }}</td>
		        	</tr>

		        </table>

	    	</div>

	    </div>

	</div>
@endsection
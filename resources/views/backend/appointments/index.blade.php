@extends('backend.layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css">
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
        	<div class="row">
        		<a href="{{ route('admin.appointments.create') }}" class="btn btn-success">Create</a>
        	</div>

        	<hr>
        	    <div id='calendar'></div>

            <hr/>

        	<table class="table table-striped table-bordered">
        		<thead>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Start Date</th>
                    <th>End Date</th>
                    <th>Message</th>
        			<td colspan="3"><center><b>Action</b></center></td>
        		</thead>
        		<tbody>
        			@foreach($appointments as $key => $appointment)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td>
							<a href="{{-- {{ route('admin.auth.user' , $appointment->patient_id)}} --}}"> 
								{{ $appointment->patient->full_name  }}
							</a>
							</td>
							<td> {{ $appointment->appointment_startdate }} </td>
                            <td> {{ $appointment->appointment_enddate }} </td>
                            <td> {{ $appointment->message }} </td>
							<td><center>{!! $appointment->action_buttons !!}</center></td>
						</tr>
					@endforeach
        		</tbody>
        	</table>
			
		</div>
	</div>
</div>
@endsection


@section('javascript')
    <script>
        @can('appointment_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.appointments.mass_destroy') }}';
        @endcan

    </script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
    <script>
        $(document).ready(function() {
            // page is now ready, initialize the calendar...
            $('#calendar').fullCalendar({
                // put your options and callbacks here
                defaultView: 'agendaWeek',
                events : [
                    @foreach($appointments as $appointment)
                    {
                        title : '{{ $appointment->patient->full_name }}',
                        start : '{{ $appointment->appointment_startdate }}',
                        @if ($appointment->appointment_enddate)
                                end: '{{ $appointment->appointment_enddate }}',
                        @endif
                        url : '{{ route('admin.appointments.edit', $appointment->id) }}'
                    },
                    @endforeach
                ]
            })
        });
    </script>
@endsection
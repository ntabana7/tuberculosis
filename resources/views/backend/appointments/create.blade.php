@extends('backend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Create Appointment') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.appointments.store') }}">
                        @csrf

                        <div class="form-group row">       
                            <label for="doctor_id" class="col-md-2 col-form-label text-md-right">{{ __('Doctors'.'*') }}</label>
                                <div class="col-md-6">
                                    {!! Form::select('doctor_id', $doctors, old('doctor_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('doctor_id'))
                                        <p class="help-block">
                                            {{ $errors->first('doctor_id') }}
                                        </p>
                                    @endif  
                                </div>
                        </div>

                        @if(isset($_GET['userId']))
                                <input id="patient_id" type="hidden" name="patient_id" value="{{ $_GET['userId']}}">
                        @else 
                        <div class="form-group row"> 
                                   
                            <label for="patient_id" class="col-md-2 col-form-label text-md-right">{{ __('Patients'.'*') }}</label>
                                <div class="col-md-6">

                                    {!! Form::select('patient_id', $patients, old('patient_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                        @if($errors->has('patient_id'))
                                            <p class="help-block">
                                                {{ $errors->first('patient_id') }}
                                            </p>
                                        @endif  
                                </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="appointment_startdate" class="col-md-2 col-form-label text-md-right">{{ __('Appointment Start Date') }}</label>

                            <div class="col-md-6">
                                <input id="appointment_startdate" type="text" class="datetime form-control{{ $errors->has('appointment_startdate') ? ' is-invalid' : '' }}" name="appointment_startdate" value="{{ old('appointment_startdate') }}" required>

                                @if ($errors->has('appointment_startdate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('appointment_startdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="appointment_enddate" class="col-md-2 col-form-label text-md-right">{{ __('Appointment End Date') }}</label>

                            <div class="col-md-6">
                                <input id="appointment_enddate" type="text" class="datetime form-control{{ $errors->has('appointment_enddate') ? ' is-invalid' : '' }}" name="appointment_enddate" value="{{ old('appointment_enddate') }}" required>

                                @if ($errors->has('appointment_enddate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('appointment_enddate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-2 col-form-label text-md-right">{{ __('message') }}</label>

                            <div class="col-md-6">
                                <textarea  id="message"  
                                    class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" 
                                    name="message" value="{{ old('message') }}" required autofocus></textarea >

                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop

@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Update Appointment') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.appointments.update', $appointment->id) }}">
                        @csrf
                        @method('PATCH')

                        <div class="form-group row">       
                            <label for="doctor_id" class="col-md-2 col-form-label text-md-right">{{ __('Doctors'.'*') }}</label>
                                

                                <div class="col-md-6">
                                    {!! Form::select('doctor_id', $doctors, old('doctor_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('doctor_id'))
                                        <p class="help-block">
                                            {{ $errors->first('doctor_id') }}
                                        </p>
                                    @endif  
                                </div>
                        </div>

                        <div class="form-group row">
                            <label for="appointment_startdate" class="col-md-2 col-form-label text-md-right">{{ __('Appointment Start Date') }}</label>

                            <div class="col-md-6">
                                <input id="appointment_startdate" type="text" class="datetime form-control{{ $errors->has('appointment_startdate') ? ' is-invalid' : '' }}" name="appointment_startdate" value="{{ $appointment->appointment_startdate }}" required>

                                @if ($errors->has('appointment_startdate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('appointment_startdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="appointment_enddate" class="col-md-2 col-form-label text-md-right">{{ __('Appointment End Date') }}</label>

                            <div class="col-md-6">
                                <input id="appointment_enddate" type="text" class="datetime form-control{{ $errors->has('appointment_enddate') ? ' is-invalid' : '' }}" name="appointment_enddate" value="{{ $appointment->appointment_enddate }}" required>

                                @if ($errors->has('appointment_enddate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('appointment_enddate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-2 col-form-label text-md-right">{{ __('message') }}</label>

                            <div class="col-md-6">
                                <textarea  id="message"  
                                    class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" 
                                    name="message" value="{{ old('message') }}" required autofocus>{{ $appointment->message }} </textarea >

                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop
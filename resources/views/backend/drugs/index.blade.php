@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
        	
        	<div class="col-sm-12"> 
            	<div class="table-responsive">

		        	<table class="table table-striped table-condensed table-hover">
		        		<thead>
		        		<tr>
		        			<td colspan="3">
		        				<a href="{{ route('admin.drugs.create') }}" class="btn btn-success">Create</a>
		        			</td>
		        		</tr>
		        		<tr>
		        			<th>#</th>
		        			<th>Name</th>
		        			<thcenter><b>Action</b></center></th>
		        		</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($drugs as $key => $drug)
								<tr> 
									<td> {{ $key + 1 }}</td>
									<td> {{ $drug->name }} </td>
									<td>{!! $drug->action_buttons !!}</td>
								</tr>
							@endforeach
		        		</tbody>
		        		<tfooter>
		        			<tr>
		        				<td colspan="5"> {{ $drugs->links()}} </td>
		        			</tr>
		        		</tfooter>
		        	</table>
		        </div>
		    </div>

		</div>
	</div>
</div>
@endsection
@extends('backend.layouts.app')

@section('content')
    <div class="card">
    <div class="card-body">

            <div class="col-md-12">
                <div class="row">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Doctor</th>
                            <td field-key='doctors'>{{ $working_hour->doctors->name ?? '' }}</td>
                        </tr>
                        <tr>
                            <th>Start Time</th>
                            <td field-key='start_time'>{{ $working_hour->start_time }}</td>
                        </tr>
                        <tr>
                            <th>Finish-Time</th>
                            <td field-key='finish_time'>{{ $working_hour->finish_time }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop

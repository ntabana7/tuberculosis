@extends('backend.layouts.app')

@section('content') 
    {!! Form::model($working_hour, ['method' => 'PUT', 'route' => ['admin.working_hours.update', $working_hour->id]]) !!}

    <div class="card">
    <div class="card-body">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Update type') }}</div>

                <div class="card-body">

                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('doctor_id', 'Doctors', ['class' => 'control-label']) !!}
                            {!! Form::select('doctor_id', $doctors, old('doctor_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('doctor_id'))
                                <p class="help-block">
                                    {{ $errors->first('doctor_id') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('start_time', 'Start Time'.'*', ['class' => 'control-label']) !!}
                            {!! Form::text('start_time', old('start_time'), ['class' => 'form-control datetime', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('start_time'))
                                <p class="help-block">
                                    {{ $errors->first('start_time') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('finish_time', 'Finish Time'.'*', ['class' => 'control-label']) !!}
                            {!! Form::text('finish_time', old('finish_time'), ['class' => 'form-control datetime', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('finish_time'))
                                <p class="help-block">
                                    {{ $errors->first('finish_time') }}
                                </p>
                            @endif
                        </div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>


    {!! Form::submit('Update', ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });
            
        });
    </script>
            
@stop
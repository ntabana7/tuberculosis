@extends('backend.layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css">
<div class="card">
    <div class="card-body">
        <div class="col-md-12">

        <div>
            <a href="{{ route('admin.working_hours.create') }}" class="btn btn-success">Create</a>
        </div>
        <div id="calendar"></div>           
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
<script>
    $(document).ready(function() {

        $('#calendar').fullCalendar({
            defaultView: 'agendaWeek',
            events : [
                
                @foreach($working_hours as $working_hour){
                    title : '{{$working_hour->doctors->fullname}}',
                    start : '{{ $working_hour->start_time}}',
                    end   : '{{$working_hour->finish_time}}',
                    url:    '{{ route('admin.working_hours.edit', $working_hour->id)}}'
                },
                @endforeach
            ]
        })
        });
</script>
@endsection
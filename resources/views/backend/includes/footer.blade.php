<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

    <div class="ml-auto">Powered by <a href="http://coreui.io">Alex</a></div>
</footer>

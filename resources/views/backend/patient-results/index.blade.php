@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
    	<h1><center><h1>List of Results</h1></center></h1>
        <div class="row"> 
        <div class="table-responsive"> 
            <div class="col-sm-6"> 		    		
		        
		        	
		        	<p><b>Firstname</b>: {{ $user->first_name }} </p>

		        	<p><b>Surname</b>:	<td>{{ $user->last_name }}</td></p>	

	    	</div>

	    	<div class="col-md-12">    	
		        <table class="table table-striped table-bordered">
	        		<thead>
	        			<tr>
	        				<th colspan="7">
	        					<a href="{{route('admin.results.create',['userId' => $usert->id])}}">
        							<span class="btn btn-success" style="color:white">Add Result</span>
        						</a>
	        				</th>
	        			</tr>
	        			<tr>
		        			<th>#</th>
		        			<th>Type</th>
		        			<th>Doctor</th>
		        			<th>Message</th>
		        			<th ><center><b>Action</b></center></th>
	        			</tr>
	        		</thead>
	        		<tbody>
	        			@foreach($patientResults as $key => $result)
							<tr> 
								<td> {{ $key + 1 }}</td>
								<td> {{ $result->type->name }} </td>
								<td> {{ $result->user->name }} </td>
								<td> {{ $result->message }} </td>
								<td><center>{!! $result->action_buttons !!}</center></td>
								
							</tr>
						@endforeach
	        		</tbody>
	        		<tfoot>
	        			<tr>
	        				<td colspan="7">
	        					{{ $patientResults->links() }}
	        				</td>
	        			</tr>
	        		</tfoot>
        		</table>
	    	</div>
		</div>
	</div>
</div>
</div>


@endsection
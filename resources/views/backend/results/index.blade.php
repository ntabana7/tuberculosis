@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">           
            <div class="table-responsive">	
                <div class="col-sm-12"> 
		        	<table class="table table-striped table-bordered table-condensed table-hover">
		        		<tr>
		        			<th>#</th>
		        			<th>Patient</th>
		        			<th>Result</th>
		        			<th>Doctor</th>
		        			<th colspan="3"><center><b>Action</b></center></th>
		        		</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($results as $key => $result)
								<tr> 
									<td> {{ $key + 1 }}</td>
									<td> {{ $result->user->first_name .' '. $result->user->last_name }} </td>
									<td> {{ $result->type->name }} </td>
									<td> {{ $result->user->name }} </td>
									<td><center>{!! $result->action_buttons !!}</center></td>
									
								</tr>
							@endforeach
		        		</tbody>
		        		<tfooter>
		        			<tr>
		        				<td colspan="7"> {{ $results->links()}} </td>
		        			</tr>
		        		</tfooter>
		        	</table>
				</div>
			</div>
	</div>
</div>
@endsection
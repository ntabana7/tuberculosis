@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">           
            <div class="table-responsive">	
                <div class="col-sm-12"> 
					<table class="table table-striped table-bordered">
						<tr>
							<td align="right">Name</td>
							<td>{{ $result->user->first_name .' '. $result->user->last_name  }}</td>
						</tr>

						<tr>
							<td align="right">Result</td>
							<td>{{ $result->type->name }}</td>
						</tr>

							<tr>
								<td align="right">Doctor</td>
								<td>{{ $result->user->name }}</td>
							</tr>

							<tr>
								<td align="right">Message</td>
								<td>{{ $result->message }}</td>
							</tr>


							<tr>
								<td align="right">Created At</td>
								<td>{{ $result->created_at }}</td>
							</tr>
						</table>
				</div>

	        	<center><h1>Prescriptions</h1></center>

		    	<div class="col-sm-12"> 	
					<table class="table table-striped table-bordered table-condensed table-hover">
		        		<thead>
		        		<tr>		        	
		        			<th  colspan="8">
		        				<a href="{{route('admin.prescriptions.create',[
		        				'userId' => $result->user_id,
		        				'resultId'  => $result->id
		        				])}}">
		        					<span class="btn btn-success" style="color:white">Add Prescription</span>
		        				</a>
			        
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>#</th>
		        			<th>Patient</th>
		        			<th>Start At</th>
		        			<th>Reorder At</th>
		        			<th colspan="3"><center><b>Action</b></center></th>
		        		</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($resultPrescriptions as $key => $prescription)
								<tr> 
									<td> {{ $key + 1 }}</td>
									<td> {{ $prescription->user->first_name .' '. $prescription->user->last_name }} </td>
									<td> {{ $prescription->start_at }} </td>
									<td> {{ $prescription->reorder_at }} </td>
									<td><center>{!! $prescription->action_buttons !!}</center></td>
								</tr>
							@endforeach
		        		</tbody>
		        		<tfooter>
		        			<tr>
		        				<td colspan="7"> {{ $resultPrescriptions->links()}} </td>
		        			</tr>
		        		</tfooter>
		        	</table>
		    	</div>
			</div>
	</div>
</div>

@endsection
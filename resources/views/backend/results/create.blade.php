@extends('backend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Create result') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.results.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="type_id" class="col-md-2 col-form-label text-md-right">{{ __('Type') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('type_id') ? ' is-invalid' : '' }}" name="type_id" required autofocus>
                                    <option value="{{old('type_id')}}">{{old('type_id')}}</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{ $type->name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="user_id" value="{{ $userId }}">

                                @if ($errors->has('type_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-2 col-form-label text-md-right">{{ __('Message') }}</label>

                            <div class="col-md-6">                               
                                <textarea id="message" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{ old('message') }}" required autofocus></textarea>

                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

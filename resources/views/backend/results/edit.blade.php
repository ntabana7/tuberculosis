@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
    		
            <div class="card">
                <div class="card-header">{{ __('Update result') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.results.update', $result->id) }}">
                        @csrf
                        @method('PATCH')

                        <div class="form-group row">
                            <label for="type_id" class="col-md-2 col-form-label text-md-right">{{ __('Type') }}</label>

                            <div class="col-md-6">
                                <input id="type_id" type="text" class="form-control{{ $errors->has('type_id') ? ' is-invalid' : '' }}" name="type_id" value="{{ $result->type_id }}" required autofocus>

                                @if ($errors->has('type_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message" class="col-md-2 col-form-label text-md-right">{{ __('Message') }}</label>

                            <div class="col-md-6">
                                <input id="message" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{ $result->message }}" required autofocus>

                               
                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
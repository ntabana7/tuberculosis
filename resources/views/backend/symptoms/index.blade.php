@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
        	<div class="row">
        		<a href="{{ route('admin.symptoms.create') }}" class="btn btn-success">Create</a>
        	</div>

        	<hr>
        	
        	<table class="table table-striped table-bordered">
        		<thead>
        			<th>#</th>
        			<th>Name</th>
        			<td colspan="3"><center><b>Action</b></center></td>
        		</thead>
        		<tbody>
        			@foreach($symptoms as $key => $symptom)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $symptom->name }} </td>
							<td> <center> {!! $symptom->action_buttons!!} </center></td>
							
						</tr>
					@endforeach
        		</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
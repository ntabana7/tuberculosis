@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="col-md-12">
        	<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Prescription</th>
        			<th>Start Date</th>
        			<th>Reorder Date</th>
        			<th>Instruction</th>
        			<th><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($prescriptions as $key => $prescription)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $prescription->user->first_name .' '. $prescription->user->last_name }} </td>
							<td> {{ $prescription->drug->name }} </td>
							<td> {{ $prescription->start_at }} </td>
							<td> {{ $prescription->reorder_at }} </td>
							<td> {{ $prescription->instruction }} </td>
							<td><center>{!! $prescription->action_buttons !!}</center></td>
						
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="7"> {{ $prescriptions->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
			
		</div>
	</div>
</div>
@endsection
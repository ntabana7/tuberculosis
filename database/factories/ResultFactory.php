<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Result::class, function (Faker $faker) {
    return [
        'user_id' => 1,//$faker->create(App\User::class)->id,
        'type_id' => 1,//$faker->create(App\Type::class)->id,
        'doctor_id' => 1,
        'message'    => $faker->text
    ];
});

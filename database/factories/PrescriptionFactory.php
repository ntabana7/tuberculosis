<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Prescription::class, function (Faker $faker) {
    return [
        'user_id'  => 1,
        'drug_id'     => 1,
        'result_id'     => 1,
        'start_at' 	  => $faker->date($format = 'Y-m-d', $max = 'now'),
        'reorder_at'  => $faker->date($format = 'Y-m-d', $max = 'now'),
        'instruction' => $faker->paragraph
    ];
});

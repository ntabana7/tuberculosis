<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Appointment::class, function (Faker $faker) {
    return [
        'patient_id' 		=> 1,
        'doctor_id' 		=> 1,
        'message' 			=> $faker->paragraph,
        'appointment_startdate' => $faker->date($format = 'Y-m-d H:i:s', $max = 'now'),
        'appointment_enddate' 	=> $faker->date($format = 'Y-m-d H:i:s', $max = 'now'), 
    ];
});

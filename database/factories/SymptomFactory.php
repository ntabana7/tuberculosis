<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Symptom::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'cache',
            'jobs',
            'sessions',
            'types',
            'drugs',
            'results',
            'symptoms',
            'appointments',
            'prescriptions',
            'working_hours'
            ]);

        $this->call(AuthTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(DrugsTableSeeder::class);
        $this->call(ResultsTableSeeder::class);
        $this->call(SymptomsTableSeeder::class);
        $this->call(PrescriptionsSeeder::class);
        $this->call(AppointmentsTableSeeder::class);

        Model::reguard();
    }
}

<?php
Breadcrumbs::for('admin.prescriptions.index', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('All Prescriptions'), route('admin.prescriptions.index'));
});


Breadcrumbs::for('admin.prescriptions.create', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('Create Prescription'), route('admin.prescriptions.create'));
});

Breadcrumbs::for('admin.prescriptions.show', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('View Prescription'), route('admin.auth.user.show', $id));
});

Breadcrumbs::for('admin.prescriptions.edit', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('Edit Prescription'), route('admin.auth.user.edit', $id));
});



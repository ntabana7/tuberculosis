<?php
Breadcrumbs::for('admin.types.index', function ($trail) {
    //$trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.type'), route('admin.types.index'));
});

Breadcrumbs::for('admin.types.edit', function ($trail, $id) {
    $trail->parent('admin.types.index');
    $trail->push(__('menus.backend.application.edit'), route('admin.types.edit', $id));
});

Breadcrumbs::for('admin.types.show', function ($trail, $id) {
    $trail->parent('admin.types.index');
    $trail->push(__('menus.backend.application.view'), route('admin.types.show', $id));
});

Breadcrumbs::for('admin.types.create', function ($trail) {
    $trail->parent('admin.types.index');
    $trail->push(__('menus.backend.application.create'), route('admin.types.create'));
});

<?php
Breadcrumbs::for('admin.symptoms.index', function ($trail) {
    //$trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.symptom'), route('admin.symptoms.index'));
});

Breadcrumbs::for('admin.symptoms.edit', function ($trail, $id) {
    $trail->parent('admin.symptoms.index');
    $trail->push(__('menus.backend.application.edit'), route('admin.symptoms.edit', $id));
});

Breadcrumbs::for('admin.symptoms.show', function ($trail, $id) {
    $trail->parent('admin.symptoms.index');
    $trail->push(__('menus.backend.application.view'), route('admin.symptoms.show', $id));
});

Breadcrumbs::for('admin.symptoms.create', function ($trail) {
    $trail->parent('admin.symptoms.index');
    $trail->push(__('menus.backend.application.create'), route('admin.symptoms.create'));
});

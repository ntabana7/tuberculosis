<?php
Breadcrumbs::for('admin.drugs.index', function ($trail) {
    //$trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.drug'), route('admin.drugs.index'));
});

Breadcrumbs::for('admin.drugs.edit', function ($trail, $id) {
    $trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.edit'), route('admin.drugs.edit', $id));
});

Breadcrumbs::for('admin.drugs.show', function ($trail, $id) {
    $trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.view'), route('admin.drugs.show', $id));
});

Breadcrumbs::for('admin.drugs.create', function ($trail) {
    $trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.create'), route('admin.drugs.create'));
});

<?php
Breadcrumbs::for('admin.appointments.index', function ($trail) {
    //$trail->parent('admin.drugs.index');
    $trail->push(__('menus.backend.application.appointment'), route('admin.appointments.index'));
});

Breadcrumbs::for('admin.appointments.edit', function ($trail, $id) {
    $trail->parent('admin.appointments.index');
    $trail->push(__('menus.backend.application.edit'), route('admin.appointments.edit', $id));
});

Breadcrumbs::for('admin.appointments.show', function ($trail, $id) {
    $trail->parent('admin.appointments.index');
    $trail->push(__('menus.backend.application.view'), route('admin.appointments.show', $id));
});

Breadcrumbs::for('admin.appointments.create', function ($trail) {
    $trail->parent('admin.appointments.index');
    $trail->push(__('menus.backend.application.create'), route('admin.appointments.create'));
});

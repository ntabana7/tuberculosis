<?php
Breadcrumbs::for('admin.patientAppointments.show', function ($trail, $id) {
    $trail->push(__('menus.backend.application.view'), route('admin.auth.user', $id));
});

Breadcrumbs::for('admin.patientAppointments.index', function ($trail, $id) {
   	$trail->parent('admin.patientAppointments.show',$id);
    $trail->push(__('menus.backend.application.appointment'), route('admin.patientAppointments.index',$id));
});

<?php
Breadcrumbs::for('admin.patient.show', function ($trail, $id) {
    $trail->push(__('menus.backend.application.view'), route('admin.auth.user', $id));
});

Breadcrumbs::for('admin.patientResults.index', function ($trail, $id) {
   	$trail->parent('admin.patient.show',$id);
    $trail->push(__('menus.backend.application.result'), route('admin.patientResults.index',$id));
});

<?php
Breadcrumbs::for('admin.working_hours.index', function ($trail) {
    //$trail->parent('admin.auth.user.index');
    $trail->push(__('All Working Hours'), route('admin.working_hours.index'));
});


Breadcrumbs::for('admin.working_hours.create', function ($trail) {
    $trail->parent('admin.working_hours.index');
    $trail->push(__('Create Working Hour'), route('admin.working_hours.create'));
});

Breadcrumbs::for('admin.working_hours.show', function ($trail, $id) {
    $trail->parent('admin.working_hours.index');
    $trail->push(__('View Working Hour'), route('admin.working_hours.show', $id));
});

Breadcrumbs::for('admin.working_hours.edit', function ($trail, $id) {
    $trail->parent('admin.working_hours.index');
    $trail->push(__('Edit Working Hour'), route('admin.working_hours.edit', $id));
});




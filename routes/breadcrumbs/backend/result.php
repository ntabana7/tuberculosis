<?php
Breadcrumbs::for('admin.results.index', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('All Results'), route('admin.prescriptions.index'));
});


Breadcrumbs::for('admin.results.create', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('Create Result'), route('admin.prescriptions.create'));
});

Breadcrumbs::for('admin.results.show', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('View Result'), route('admin.auth.user.show', $id));
});

Breadcrumbs::for('admin.results.edit', function ($trail, $id) {
    $trail->parent('admin.auth.user.index');
    $trail->push(__('Edit Result'), route('admin.auth.user.edit', $id));
});




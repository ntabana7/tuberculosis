<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

require __DIR__.'/auth.php';
require __DIR__.'/drug.php';
require __DIR__.'/type.php';
require __DIR__.'/result.php';
require __DIR__.'/symptom.php';
require __DIR__.'/log-viewer.php';
require __DIR__.'/workingHour.php';
require __DIR__.'/appointment.php';
require __DIR__.'/prescription.php';
require __DIR__.'/patientResult.php';
require __DIR__.'/patientPrescription.php';
require __DIR__.'/patientAppointment.php';




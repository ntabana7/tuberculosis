<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\PatientResultsController;
use App\Http\Controllers\Backend\PatientAppointmentsController;
use App\Http\Controllers\Backend\PatientPrescriptionsController;
/*
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
route::resource('drugs', 'DrugsController');
route::resource('types', 'TypesController');
route::resource('results', 'ResultsController');
route::resource('symptoms', 'SymptomsController');
route::resource('appointments', 'AppointmentsController');
route::resource('prescriptions', 'PrescriptionsController');

route::get('patient-results/{user}', [PatientResultsController::class,'index'])
		->name('patientResults.index');

route::get('patient-appointments/{user}', [PatientAppointmentsController::class,'index'])
		->name('patientAppointments.index');

route::get('patient-prescriptions/{user}', [PatientPrescriptionsController::class,'index'])
		->name('patientPrescriptions.index');

Route::resource('working_hours', 'WorkingHoursController');
Route::post('working_hours_mass_destroy', ['uses' => 'WorkingHoursController@massDestroy', 'as' => 'working_hours.mass_destroy']);
Route::post('working_hours_restore/{id}', ['uses' => 'WorkingHoursController@restore', 'as' => 'working_hours.restore']);
Route::delete('working_hours_perma_del/{id}', ['uses' => 'WorkingHoursController@perma_del', 'as' => 'working_hours.perma_del']);

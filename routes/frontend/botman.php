<?php
use App\Conversations\TestConversation;

$botman = resolve('botman');

$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello!');
});

$botman->hears('Call me {name}', function ($bot , $name){
	$bot->reply('my name is '.$name);
});

$botman->hears('I want ([0-9]+)', function ($bot, $number) {
    $bot->reply('You will get: '.$number);
});

$botman->hears('I want ([0-9]+) portions of (Cheese|Cake)', function ($bot, $amount, $dish) {
    $bot->reply('You will get '.$amount.' portions of '.$dish.' served shortly.');
});

$botman->hears('.*Bonjour.*', function ($bot) {
    $bot->reply('Nice to meet you!');
});

$botman->hears('test' , function ($bot) {
	$bot->startConversation(new TestConversation);
});


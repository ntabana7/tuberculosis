<?php

namespace App\Http\Controllers\frontend\User;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        return view('frontend.appointments.index')
            ->withAppointments(auth()->user()->appointmentsByPatient()->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('frontend.appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'symptom_id'        => ['required' , 'integer'],
            'medication'        => ['required' , 'min:3'],
            'message'           => ['required' , 'string','min:3'],
            'appointment_date'  => ['required' , 'date']
        ]);
        
        $attributes['user_id'] = auth()->id();

        Appointment::create($attributes);

        //$patient = \App\Models\Patient::where(['id' => auth()->id()])->get();
        //$patient->notify(new AppointmentDue);

        return redirect('/appointments');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
         return view('frontend.appointments.show')->withAppointment($appointment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        return view('frontend.appointments.edit')->withAppointment($appointment);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
         $appointment->update(
            [
                'message'           => $request->message,
                'medication'        => $request->medication,
            ]);

        return redirect('/appointments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        $appointment->delete();

        return redirect('/appointments');
    }
}

<?php

namespace App\Http\Controllers\frontend\User;

use App\Models\Prescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrescriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function show(Prescription $prescription)
    {
        return view('frontend.prescriptions.show')
            ->withPrescription($prescription);
    }
}

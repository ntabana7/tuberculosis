<?php

namespace App\Http\Controllers\Backend;

use App\Models\Drug;
use App\Models\Prescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrescriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.prescriptions.index')->withPrescriptions(Prescription::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.prescriptions.create')
                    ->withUserId($_GET['userId'])
                    ->withResultId($_GET['resultId'])
                    ->withDrugs(Drug::all());;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validatedPrescription();

        $attributes['user_id'] = $request->user_id;
        $attributes['result_id']  = $request->result_id;
         

        Prescription::create($attributes);

        return redirect()->route('admin.auth.user.show', ['id' => $attributes['user_id']])
                         ->withFlashSuccess(__('The Prescription was successfully Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function show(Prescription $prescription)
    {
        return view('backend.prescriptions.show')->withPrescription($prescription);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function edit(Prescription $prescription)
    {
        return view('backend.prescriptions.edit')->withPrescription($prescription);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prescription $prescription)
    {
        $validatePrescription = $this->validatedPrescription();

        $prescription->update($validatePrescription);

    return redirect()->route('admin.auth.user.show', ['id' => $prescription->user_id])
                         ->withFlashSuccess(__('The Prescription was successfully Updated'));        
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prescription $prescription)
    {
        $prescription->delete();

        return redirect()->route('admin.auth.user.show', ['id' => $prescription->user_id])
                         ->withFlashSuccess(__('The Prescription was successfully Deleted'));
    
    }

     protected function validatedPrescription() {
        return request()->validate([
            'drug_id'     => ['required' , 'integer'],
            'start_at'    => ['required', 'date'],
            'reorder_at'  => ['required', 'date'],
            'instruction' => ['required' , 'min:3']
        ]);
    }
}

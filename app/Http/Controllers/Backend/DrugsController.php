<?php

namespace App\Http\Controllers\Backend;

use App\Models\Drug;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DrugsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.drugs.index')->withDrugs(Drug::orderBy('name')->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.drugs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required' , 'min:3']
        ]);

        $drug = Drug::create(request(['name']));

        return redirect('admin/drugs')->withFlashSuccess(__('Drug was successfully created'));;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Drug  $drug
     * @return \Illuminate\Http\Response
     */
    public function show(Drug $drug)
    {
        return view('backend.drugs.show')->withDrug($drug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Drug  $drug
     * @return \Illuminate\Http\Response
     */
    public function edit(Drug $drug)
    {
        return view('backend.drugs.edit')->withDrug($drug);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Drug  $drug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Drug $drug)
    {
        $drug->update(['name' => $request->name]);

        return redirect('/admin/drugs')->withFlashSuccess(__($drug->name.' was successfully updated'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Drug  $drug
     * @return \Illuminate\Http\Response
     */
    public function destroy(Drug $drug)
    {
        $drug->delete();

        return redirect('/admin/drugs')->withFlashSuccess(__($drug->name.' was successfully updated'));

    }
}

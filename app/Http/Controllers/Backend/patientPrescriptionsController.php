<?php

namespace App\Http\Controllers\Backend;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class patientPrescriptionsController extends Controller
{
    public function index(Patient $patient)
    {
        return view('backend.patient-prescriptions.index',[
            'patient' => $patient ,
            'patientPrescriptions' => $patient->prescriptions()->orderBy('id', 'DESC')->paginate( 4 , ['*'] , '2page')
        ]);
                
    }
}

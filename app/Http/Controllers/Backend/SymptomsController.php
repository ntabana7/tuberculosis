<?php

namespace App\Http\Controllers\Backend;

use App\Models\Symptom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SymptomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.symptoms.index')->withSymptoms(Symptom::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.symptoms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required' , 'min:3']
        ]);

        Symptom::create(request(['name']));

        return redirect('/admin/symptoms')->withFlashSuccess(__('Symptom was successfully created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function show(Symptom $symptom)
    {
        return view('backend.symptoms.show')->withSymptom($symptom);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function edit(Symptom $symptom)
    {
        return view('backend.symptoms.edit')->withSymptom($symptom);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Symptom $symptom)
    {
        $symptom->update(['name' => $request->name]);

        return redirect('/admin/symptoms')->withFlashSuccess(__($symptom->name.' was successfully updated'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Symptom  $symptom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Symptom $symptom)
    {
        //$symptom->delete();

        return redirect('/admin/symptoms')
        ->withFlashDanger(__('You are not allowed to Deleted the result'));
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class patientResultsController extends Controller
{
    public function index(Patient $patient)
    {
        return view('backend.patient-results.index',[
            'patient' => $patient ,
            'patientResults' => $patient->results()->paginate( 10 , ['*'] , '1page' ),
        ]);
                
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Models\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.types.index')->withTypes(Type::orderBy('name')->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData  = request()->validate(
            [
                'name' => ['required','string','min:3']
            ]);

        Type::create($validatedData );

        return redirect('/admin/types')->withFlashSuccess(__($validatedData['name'].' was successfully created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Types  $types
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        return view('backend.types.show')->withType($type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Types  $types
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        
        return view('backend.types.edit')->withType($type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Types  $types
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $validatedData  = request()->validate([
            'name' => ['required' , 'min:3' , 'unique:types']
        ]);

        $type->update($validatedData );

        return redirect('/admin/types')->withFlashSuccess(__($type->name.' was successfully updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Types  $types
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        //$type->delete();

        return redirect('/admin/types')->withFlashDanger(__('You are not allowed to Deleted the type'));
    }
}

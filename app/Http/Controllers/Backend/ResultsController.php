<?php

namespace App\Http\Controllers\Backend;

use App\Models\Type;
use App\Models\Result;
use App\Mail\ResultCreated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ResultNotification;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.results.index')->withResults(auth()->user()->results()->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('backend.results.create')
                    ->withUserId($_GET['userId'])
                    ->withTypes(Type::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validatedResult();

        $attributes['doctor_id'] = auth()->id();


        $result = Result::create($attributes);

        $patient = \App\Models\Auth\User::where(['id' => $attributes['user_id'] ])->first();

        \Mail::to($patient->email)->send(
            new ResultCreated($result)
        );
        
        $patient->notify(new ResultNotification());

        return redirect()->route('admin.auth.user.show', ['id' => $attributes['user_id']])
                         ->withFlashSuccess(__('The Result was successfully Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
       return view('backend.results.show')
       ->withResult($result)
       ->withResultPrescriptions($result->prescriptions()->paginate( 10 , ['*'] , '1page' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result)
    {
        return view('backend.results.edit')->withResult($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
        $attributes = $this->validatedResult();

        $result->update($attributes);

       return redirect()->route('admin.auth.user.show', ['id' => $result->user_id])
                         ->withFlashSuccess(__('The Result was successfully Updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        $result->delete();

        return  \Redirect::route('admin.auth.user.show', ['id' => $appointment->user_id]);
    }

    protected function validatedResult() {
        return request()->validate([
            'user_id' => ['integer'],
            'type_id' => ['required', 'integer'],
            'message' => ['required', 'min:5']
        ]);
    }
}

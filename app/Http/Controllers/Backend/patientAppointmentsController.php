<?php

namespace App\Http\Controllers\Backend;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class patientAppointmentsController extends Controller
{
    public function index(Patient $patient)
    {
    	
        return view('backend.patient-appointments.index',[
            'patient' => $patient ,
            'patientAppointments' => $patient->appointments()->orderBy('id', 'DESC')->paginate( 4 , ['*'] , '3page')
        ]);
                
    }
}

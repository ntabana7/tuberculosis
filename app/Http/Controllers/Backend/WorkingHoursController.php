<?php

namespace App\Http\Controllers\Backend;

use App\Models\Auth\User;
use App\Models\WorkingHour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\UserRepository;
use App\Http\Requests\backend\StoreWorkingHoursRequest;
use App\Http\Requests\backend\UpdateWorkingHoursRequest;

class WorkingHoursController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of WorkingHour.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        
        $working_hours = WorkingHour::all();

        return view('backend.working_hours.index', compact('working_hours'));
    }

    /**
     * Show the form for creating new WorkingHour.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
               
        $doctors  = $this->userRepository->getDoctorActivePaginated(25, 'id', 'asc')->pluck('name', 'id')->prepend('select', '');
        
        return view('backend.working_hours.create', compact('doctors'));
    }

    /**
     * Store a newly created WorkingHour in storage.
     *
     * @param  \App\Http\Requests\StoreWorkingHoursRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorkingHoursRequest $request)
    {
        
        $working_hour = WorkingHour::create($request->all());

        return redirect()->route('admin.working_hours.index');
    }


    /**
     * Show the form for editing WorkingHour.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
               
        $doctors  = $this->userRepository->getDoctorActivePaginated(25, 'id', 'asc')->pluck('name', 'id')->prepend('select', '');

        $working_hour = WorkingHour::findOrFail($id);

        return view('backend.working_hours.edit', compact('working_hour', 'doctors'));
    }

    /**
     * Update WorkingHour in storage.
     *
     * @param  \App\Http\Requests\UpdateWorkingHoursRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkingHoursRequest $request, $id)
    {

        $working_hour = WorkingHour::findOrFail($id);
        $working_hour->update($request->all());

        return redirect()->route('admin.working_hours.index');
    }


    /**
     * Display WorkingHour.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
        $working_hour = WorkingHour::findOrFail($id);

        return view('backend.working_hours.show', compact('working_hour'));
    }


    /**
     * Remove WorkingHour from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $working_hour = WorkingHour::findOrFail($id);
        $working_hour->delete();

        return redirect()->route('admin.working_hours.index');
    }

    /**
     * Delete all selected WorkingHour at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
     
        if ($request->input('ids')) {
            $entries = WorkingHour::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore WorkingHour from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
          
        $working_hour = WorkingHour::onlyTrashed()->findOrFail($id);
        $working_hour->restore();

        return redirect()->route('admin.working_hours.index');
    }

    /**
     * Permanently delete WorkingHour from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
  
        $working_hour = WorkingHour::onlyTrashed()->findOrFail($id);
        $working_hour->forceDelete();

        return redirect()->route('admin.working_hours.index');
    }
}

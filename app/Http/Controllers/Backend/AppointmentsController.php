<?php

namespace App\Http\Controllers\Backend;

use App;
use App\Models\Auth\User;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\AppointmentApproved;
use App\Notifications\Backend\AppointmentDue;
use App\Repositories\Backend\Auth\UserRepository;

class AppointmentsController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.appointments.index')->withAppointments(Appointment::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $doctors  = $this->userRepository->getDoctorActivePaginated(25, 'id', 'asc')->pluck('name', 'id')->prepend('select', '');
        $patients = $this->userRepository->getPatientActivePaginated(25, 'id', 'asc')->pluck('name', 'id')->prepend('select', '');
        return view('backend.appointments.create')->withDoctors($doctors)
                                                  ->withPatients($patients);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'patient_id'             => ['required'], 
            'doctor_id'              => ['required'],
            'appointment_startdate'  => ['required' , 'date'],
            'appointment_enddate'    => ['required' , 'date'],
            'message'                => ['required' , 'string','min:3'],
        ]);
      
        Appointment::create($attributes);

        $patient = App\Models\Auth\User::where(['id' => $attributes['patient_id'] ])->first();
        $patient->notify(new AppointmentDue);
        
        $patient->notify(new AppointmentApproved($attributes));
        return  redirect()->route('admin.auth.user.show', ['id' => $patient->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
    
        return view('backend.appointments.show')->withAppointment($appointment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        $doctors  = $this->userRepository->getDoctorActivePaginated(25, 'id', 'asc')->pluck('name', 'id')->prepend('select', '');
        return view('backend.appointments.edit')->withAppointment($appointment)
                                                ->withDoctors($doctors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    { 

        $attributes = request()->validate([
            'doctor_id'              => ['required'],
            'appointment_startdate'  => ['required' , 'date'],
            'appointment_enddate'    => ['required' , 'date'],
            'message'                => ['required' , 'string','min:3'],
        ]);

        $appointment->update($attributes);

        return redirect('/admin/appointments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        $appointment->delete();

        return  \Redirect::route('admin.auth.user.show', ['id' => $appointment->user_id]);
    }
}

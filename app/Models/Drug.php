<?php

namespace App\Models;

use App\Models\Auth\Traits\Attribute\DrugAttribute;
use Illuminate\Database\Eloquent\Model;
use App\Events\DrugCreated;
use Mail;

class Drug extends Model
{
	use DrugAttribute;
	
    protected $guarded = [];

    protected $dispatchesEvents = [
        'created' => DrugCreated::class
    ];

    public function prescriptions() {
    	 return $this->hasMany(Prescription::class);
    }
}

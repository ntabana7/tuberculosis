<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Attribute\TypeAttribute;

class Type extends Model
{
	use TypeAttribute;

    protected $guarded = [];

}

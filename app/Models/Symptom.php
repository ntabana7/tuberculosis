<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Attribute\SymptomAttribute;

class Symptom extends Model
{
	use SymptomAttribute;

    protected $guarded = [];

    public function appointment() {

    	return $this->hasMany(Appointment::class);

    }
}

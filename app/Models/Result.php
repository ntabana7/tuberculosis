<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Attribute\ResultAttribute;
use App\Models\Auth\User;

class Result extends Model
{
    use ResultAttribute;

    protected $guarded = [];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function doctor(){
        return $this->belongsTo(User::class,'doctor_id');
    }

    public function type(){
    	return $this->belongsTo(Type::class);
    }

    public function prescriptions(){
        return $this->hasMany(Prescription::class);
    }
}

<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Attribute\PrescriptionAttribute;

class Prescription extends Model
{
	use PrescriptionAttribute;

    protected $guarded = [];

    public function user() {
    	return $this->belongsTo(User::class);
    }

    public function drug(){
    	return $this->belongsTo(Drug::class);
    }

    public function result(){
    	return $this->belongsTo(Result::class);
    }
}

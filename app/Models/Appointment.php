<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Attribute\AppointmentAttribute;

class Appointment extends Model
{
	use AppointmentAttribute;

    protected $guarded = [];

    public function symptom () {

    	return $this->belongsTo(Symptom::class);

    }

    public function patient () {

    	return $this->belongsTo(User::class , 'patient_id');

    }
}

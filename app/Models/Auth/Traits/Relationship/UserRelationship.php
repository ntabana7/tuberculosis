<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Result;
use App\Models\Appointment;
use App\Models\Prescription;
use App\Models\System\Session;
use App\Models\Auth\SocialAccount;
use App\Models\Auth\PasswordHistory;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{

    public function results() {
        return $this->hasMany(Result::class);
    }

    public function prescriptions() {
        return $this->hasMany(Prescription::class);
    }

    public function appointmentsByPatient() {
        return $this->hasMany(Appointment::class,'patient_id');
    }

    public function appointmentsForDoctor() {
        return $this->hasMany(Appointment::class,'doctor_id');
    }
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    public function workingHours() {
        return $this->hasMany(workingHours::class,'doctor_id');
    }
}

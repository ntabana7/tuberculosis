<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class DrugCreated
{
    use Dispatchable, SerializesModels;
    
    public $drug;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($drug)
    {
        $this->drug = $drug;
    }

}

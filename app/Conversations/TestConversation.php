<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;

class TestConversation extends Conversation
{
    protected $name;
    protected $email;
    protected $title;
    protected $genre;
    protected $year;

    public function stopsConversation(IncomingMessage $message)
	{
		if ($message->getText() == 'stop') {
			return true;
		}

		return false;
	}


	public function skipsConversation(IncomingMessage $message)
	{
		if ($message->getText() == 'pause') {
			return true;
		}

		return false;
	}


    public function askName() {

    	$this->ask("Hello What's your name ?" , function (Answer $answer) {

    		$this->bot->userStorage()->save([
                'name' => $answer->getText(),
            ]);
    		$this->name = $answer->getText();
    		$this->say("Nice to meet you ".$this->name);

    		$this->askEmail();
    	});
    }

    public function askEmail() {

    	$this->ask("What is your email ?", function (Answer $answer) {

    		$validator = \Validator::make(['email' => $answer->getText()], [
            	'email' => 'email',
	        ]);

	        if ($validator->fails()) {
	            return $this->repeat('That doesn\'t look like a valid email. Please enter a valid email.');
	        }

	        $this->bot->userStorage()->save([
            	'email' => $answer->getText(),
        	]);

    		$this->say("Thank you. ");
    		$this->say("Other few question");

    		$this->askTitle();
    	});
    }

    public function askTitle() {

    	$this->ask("What the name of movie do you what ?" , function (Answer $answer) {
    		
    		$this->bot->userStorage()->save([
                'title' => $answer->getText(),
            ]);

    		$this->say("Two more Questions");

    		$this->askYear();
    	});
    }

    public function askYear() {

    	$this->ask("Which year ?" , function (Answer $answer) {

    		$this->bot->userStorage()->save([
                'year' => $answer->getText(),
            ]);

    		$this->say("You are lucky enough.We have that movie in our store");

    		$this->askForDatabase();
    	});
    }

    public function askForDatabase()
	{
    $question = Question::create('Do you need a database?')
        ->fallback('Unable to create a new database')
        ->callbackId('create_database')
        ->addButtons([
            Button::create('Of course')->value('yes'),
            Button::create('Hell no!')->value('no'),
        ]);

    $this->ask($question, function (Answer $answer) {
        // Detect if button was clicked:
        if ($answer->isInteractiveMessageReply()) {
            $selectedValue = $answer->getValue(); // will be either 'yes' or 'no'
            $selectedText = $answer->getText(); // will be either 'Of course' or 'Hell no!'
        }
    });
}
    
    public function run()
    {
        $this->askName();
    }
}
